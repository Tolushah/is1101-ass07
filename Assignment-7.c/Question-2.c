/*Index no :- 19020325
Harindi.M.A.T.
Assignment 7 - Question 2*/


#include <stdio.h>
#define size 100

//PROGRAM TO FIND THE FREQUENCY OF A GIVEN CHARACTER

//frequency function
void frequency(char freq[]);

void frequency(char freq[]){
	int i, count=0;
	char character;
	
	printf("Enter a character to find its frequency : ");
	scanf("%c", &character);
	
	for(i=0; freq[i]!='\0'; i++){
		if(character==freq[i])
		count++;
	}
	 printf("Frequency of %c = %d", character,count);
}
 
 int main(){

    char word[size];
 	printf("Enter a sentence: ");
 	fgets(word, size, stdin);
    frequency(word);
 	return 0;
 }
