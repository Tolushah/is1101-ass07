/*Index no :- 19020325
Harindi.M.A.T.
Assignment 7 - Question 3*/


#include <stdio.h>
#define size 20
//program to add and multiply two given matrices

// addition function
void addition(int first[size][size], int second[size][size], int rows1, int cols1);
// multiplication function
void multiplication(int first[size][size], int second[size][size], int rows1, int cols1, int cols2);
//input function
void input(int matrix[size][size], int rows, int cols);
//display function
void display(int matrix[size][size], int rows, int cols);

int rows1, cols1, rows2, cols2, i,j,k, rows, cols;
int first[size][size], second[size][size];
	
void input(int matrix[size][size], int rows, int cols){
	//getting elements of matrices
	for (i=0; i<rows; i++)
		for (j=0; j<cols; j++)
		    scanf("%d", &matrix[i][j]);
		}


void display(int matrix[size][size], int rows, int cols){
    //printing results of addition and multiplication
	for ( i=0; i<rows; i++){
     for (j=0; j<cols; j++){
      printf("%d\t", matrix[i][j]);
     }printf("\n");
    }
}

void addition(int first[size][size], int second[size][size], int rows1, int cols1){
	
	int addmatrix[size][size];
    printf("\nAddition\n");
    //addition process
    for (i=0; i<rows1; i++){
     for (j=0; j<cols1; j++){
      addmatrix[i][j] = first[i][j] + second[i][j]; //adding two matrices
     }
    }
    display(addmatrix, rows1, cols1);
}

void multiplication(int first[size][size], int second[size][size], int rows1, int cols1, int cols2){
	
	int multimatrix[size][size];
    printf("\nMultiplication\n");
    //multiplication process
    for (i=0; i<rows1; i++){
     for (j=0; j<cols2; j++){
     	multimatrix[i][j]=0;
      for ( k=0; k<cols1; k++){
      multimatrix[i][j] +=(first[i][k] * second[k][j]); //multiplying two matrices
      }
     }
    }
    display(multimatrix, rows1, cols2);
}


int main(){
		
		printf("\n*Enter the number of rows and columns for 1st matrix:\n");
		scanf("%d \n %d",&rows1, &cols1);
		
		printf("\n*Enter the number of rows and columns for 2nd matrix:\n");
		scanf("%d \n %d",&rows2, &cols2);
		
		printf("\n*Enter elements of 1st matrix:\n");
		input(first,rows1, cols1);
		
		printf("\n*Enter elements of 2nd matrix:\n");
		input(second, rows2, cols2);
		
		//checking conditions before doing addition and multiplication
		if((rows1==rows2) && (cols1==cols2))
		{
			addition(first, second, rows1, cols1);
		}
		else {
			printf("\n*Dimentions should be equal to do add operation\n");
		}
		if(cols1==rows2){
			multiplication(first, second, rows1, cols1, cols2);
		}
		else {
			printf("\n*No of columns of First matrix must be eqaul to the no of rows of second matrix\n");
		}
		return 0;
}


