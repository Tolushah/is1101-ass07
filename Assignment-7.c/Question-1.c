/*Index no :- 19020325
Harindi.M.A.T.
Assignment 7 - Question 1*/


#include <stdio.h>
#include <string.h>
#define size 100

// PROGRAM TO REVERSE A SENTENCE

//reverse function
void reverse(char re[]);

int main(){
		
		char sentence[size];
		printf("Enter a sentence: ");  
		fgets(sentence, size, stdin); //function to read string from user
	 	reverse(sentence); //calling to reverse function
		return 0;
}

//reverse process
void reverse(char re[]){
		int i;
		for(i=strlen(re)-1; i>=0; i--){
			
			//printing reverse sentence
			printf("%c",re[i]);
	}
}
